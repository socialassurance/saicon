require_relative "lib/saicon/version"

Gem::Specification.new do |spec|
  spec.name        = "saicon"
  spec.version     = Saicon::VERSION
  spec.authors     = ["Adam"]
  spec.email       = ["adam@socialassurance.com"]
  spec.homepage    = "https://socialassurance.com"
  spec.summary     = "Saicon"
  spec.description = "Saicon"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  #spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"

  #spec.metadata["homepage_uri"] = spec.homepage
  #spec.metadata["source_code_uri"] = "TODO: Put your gem's public repo URL here."
  #spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", ">= 6.1"

  spec.add_development_dependency "appraisal"
  spec.add_development_dependency "byebug"
  spec.add_development_dependency "standard"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "mocha"
end
