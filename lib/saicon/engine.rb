# frozen_string_literal: true

module Saicon
  class Engine < ::Rails::Engine
    isolate_namespace Saicon
  end
end

