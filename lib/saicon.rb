require "saicon/version"
require "saicon/railtie"
require "saicon/engine"
require "saicon/icon"

module Saicon
  def self.root
    File.dirname(__dir__)
  end
end
