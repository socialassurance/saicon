module Saicon
  class Icon
    attr_reader :name, :options

    def initialize(name:, options:)
      @name = name
      @options = options || {}
    end

    def render
      return warning unless file.present?

      doc = Nokogiri::HTML::DocumentFragment.parse(file)
      svg = doc.at_css "svg"

      prepend_default_class_name
      prepend_color_class_name
      prepend_size_class_name

      options.each do |key, value|
        svg[key.to_s] = value
      end
      
      doc
    end

    private

    def prepend_default_class_name
      @options[:class] = "fill-current #{options[:class]}".strip 
    end

    def prepend_color_class_name
      @options[:class] = "text-#{options[:color]} #{options[:class]}".strip if options[:color].present?
    end

    def prepend_size_class_name
      # Translate non-number sizes
      size = {xs: 2,
              sm: 4,
              md: 6,
              lg: 8}[options[:size]] || options[:size] || 6

      @options[:class] = "w-#{size} h-#{size} #{options[:class]}".strip 
    end

    def library
      @library ||= (options[:library] || :material).to_sym
    end

    def file
      @@cache = {}
      @@cache[file_path] ||= read_file(file_path) 
    rescue
      @@cache[file_path] ||= nil
    end

    def file_path
      return sa_path if library == :sa 
      return ionicon_path if library == :ionicons
      material_path
    end

    def read_file(path)
      File.read(path).force_encoding("UTF-8")
    end

    def sa_path
      File.join(Saicon.root, "app/assets/images/sa/#{name}.svg")
    end

    def ionicon_path
      File.join(Saicon.root, "app/assets/images/ionicons/#{name}.svg")
    end

    def material_path
      File.join(Saicon.root, "app/assets/images/material/#{name}/#{material_variant}/24px.svg")
    end

    def material_variant
      variant = @options[:variant] || ''
      "materialicons#{variant}"
    end

    def warning
      return unless Rails.env.development?

      script = <<-HTML
      <script type="text/javascript">
      //<![CDATA[
      console.warn("Saicon: Failed to find saicon: #{name}")
      //]]>
      </script>
      HTML

      script.strip
    end

    class << self
      def render(**kwargs)
        new(**kwargs).render
      end
    end
  end
end
