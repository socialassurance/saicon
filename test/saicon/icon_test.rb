require "test_helper"
require "byebug"

class Saicon::IconTest < ActiveSupport::TestCase
  test "returns a Nokogiri::HTML::DocumentFragment" do
    assert_kind_of Nokogiri::HTML::DocumentFragment, default_icon
  end

  test "adds default fill class" do
    svg = default_icon.at_css "svg"
    assert_equal svg['class'], "fill-current"
  end

  test "can change color" do
    svg = default_icon(options: {color: 'gray-200'}).at_css "svg"
    assert_equal svg['class'], "text-gray-200 fill-current"
  end

  test "can change size" do
    svg = default_icon(options: {size: 6}).at_css "svg"
    assert_equal svg['class'], "w-6 h-6 fill-current"
  end

  test "can add class" do
    svg = default_icon(options: {class: 'ml-6'}).at_css "svg"
    assert_equal svg['class'], "fill-current ml-6"
  end

  def default_icon(options: {})
    Saicon::Icon.new(name: :person, options: options).render
  end
end
