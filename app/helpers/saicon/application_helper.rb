# frozen_string_literal: true

module Saicon
  module ApplicationHelper
    def saicon(name, **kwds)
      raw Saicon::Icon.render(
        name: name,
        options: kwds
      )
    end
  end
end
