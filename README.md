# Saicon
Icons used by Social Assurance products

## Usage
```
saicon('person', color: :primary, size: 12)
```

## Installation
Add this line to your application's Gemfile:


```ruby
gem 'saicon', git: "https://bitbucket.org/socialassurance/saicon.git", branch: 'main'
```

And then execute:
```bash
$ bundle
```

Then create a `app/helpers/saicon_helper.rb` file with:
```
module SaiconHelper
  include Saicon::Engine.helpers
end
```
